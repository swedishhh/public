import os
from setuptools import setup

setup(
    name = "dej.poloniex",
    version = "0.0.1",
    author = "",
    author_email = "",
    description = "",
    license = "",
    keywords = "",
    url = "",
    packages=['dej.poloniex'],
    long_description='',
    install_requires=[
        'dej.base',
    ]
)
