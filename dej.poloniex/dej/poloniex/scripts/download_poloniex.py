import logging
from optparse import OptionParser
from dej.poloniex.app import download

logging.basicConfig(level='INFO')
logger = logging.getLogger(__name__)


if __name__ == '__main__':

    parser = OptionParser()
    
    parser.add_option('-a', '--arctic_host',
        dest='arctic_host', default='localhost', help='Arctic host')
    parser.add_option('-l', '--library',
        dest='library', default='polo.prices', help='Arctic library name')
    parser.add_option('--symbols',
        dest='symbols', default=None, help='Comma separated list of symbols to download')
    parser.add_option('-d', '--include_delisted',
        dest='include_delisted', default=False, action='store_true', help='Include delisted symbols')
    parser.add_option('-o', '--force_overwrite',
        dest='force_overwrite', default=False, action='store_true', help='Force an overwrite for all downloaded symbols')
    
    (options, args) = parser.parse_args()

    logger.info('options=%s' % options)
    
    if options.symbols is not None:
        options.symbols = options.symbols.split(',')

    logger.info('options=%s' % options)

    download(
        options.arctic_host,
        options.library,
        options.symbols,
        options.include_delisted,
        options.force_overwrite)
