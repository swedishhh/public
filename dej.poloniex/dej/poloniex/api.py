from dej.poloniex.util import convert_floats

try:
    # For Python 3.0 and later
    from urllib.request import urlopen, Request
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen, Request
    from urllib import urlencode

import json
import time, datetime
from datetime import date, datetime
import calendar
import hmac,hashlib
from pandas import DataFrame
import pandas as pd


def convert_chart_data_to_dataframe(x):
    df = DataFrame(x)
    df = df.assign(timestamp=pd.to_datetime(df['date'], unit='s'))
    columns = ['timestamp', 'date', 'open', 'high', 'low', 'close', 'volume', 'quoteVolume', 'weightedAverage']
    df = df[columns]
    df = df.set_index('timestamp')
    return df


def create_timestamp(t, format="%Y-%m-%d %H:%M:%S"):
    """
    createTimeStamp('2016-04-05 08:08:40')
    :param t: 
    :param format: 
    :return: 
    """
    if hasattr(t, 'year'):
        return calendar.timegm(t.timetuple())

    if t and type(t) is str:
        return time.mktime(time.strptime(t, format))

    raise Exception('unrecognised type for conversion to UNIX timestamp (%s)' % t)

class Poloniex:
    def __init__(self, APIKey=None, Secret=None, parseJson=True, post_process=True):
        self.APIKey = APIKey
        self.Secret = Secret
        self.parseJson = parseJson
        self.post_process = post_process

    # PUBLIC API METHODS

    def returnTicker(self):
        """
        Returns the ticker for all markets
        :return: 
        """
        return self._public("returnTicker")

    def return24hVolume(self):
        """
        Returns the 24-hour volume for all markets, plus totals for primary currencies.
        :return: 
        """
        return self._public("return24hVolume")

    def returnOrderBook (self, currencyPair, depth=10):
        """
        Returns the order book for a given market, as well as a sequence number for use with the Push API
        and an indicator specifying whether the market is frozen.
        You may set currencyPair to "all" to get the order books of all markets.
        :param currencyPair: 
        :param depth: 
        :return: 
        """
        return self._public("returnOrderBook", {'currencyPair': currencyPair, 'depth': depth})

    def returnTradeHistory (self, currencyPair, start=None, end=None):
        """
        Returns the past 200 trades for a given market, or up to 50,000 trades between a range
        specified in UNIX timestamps by the "start" and "end" GET parameters.
        :param currencyPair: 
        :param start: YYYY-MM-DD HH:MM:SS
        :param end: YYYY-MM-DD HH:MM:SS
        :return: 
        """
        return self._public("returnTradeHistory", {
            'currencyPair': currencyPair,
            'start': create_timestamp(start),
            'end': create_timestamp(end)
        })

    def returnChartData (self, currencyPair, start=None, end=None, period=300, as_df=True):
        """
        Returns candlestick chart dej.poloniex.
        Required GET parameters are
        "currencyPair",
        "period" (candlestick period in seconds; valid values are 300, 900, 1800, 7200, 14400, and 86400),
        "start"
        "end".
        "Start" and "end" are given in UNIX timestamp format and used to specify the
        date range for the dej.poloniex returned.
        :param currencyPair: 
        :param period: 
        :param start: YYYY-MM-DD HH:MM:SS
        :param end: YYYY-MM-DD HH:MM:SS
        :return: 
        """
        data= self._public("returnChartData", {
            'currencyPair': currencyPair,
            'start': create_timestamp(start),
            'end': create_timestamp(end),
            'period': period
        })
        if as_df:
            return convert_chart_data_to_dataframe(data)
        else:
            return data


    def returnCurrencies(self):
        """
        Returns information about currencies
        :return: 
        """
        return self._public("returnCurrencies")


    def returnLoanOrders(self, currency):
        """
        Returns the list of loan offers and demands for a given currency, specified by the "currency" GET parameter
        :param currency: 
        :return: 
        """
        return self._public("returnLoanOrders", {'currency' : currency})


    # TRADING API METHODS

    def returnBalances(self):
        """
        Returns all of your balances.
        Outputs:
        {"BTC":"0.59098578","LTC":"3.31117268", ... }
        :return: 
        """
        return self._private('returnBalances')


    def returnCompleteBalances(self):
        """
        Returns all of your balances, including available balance, balance on orders, and the estimated BTC value of your balance.
        By default, this call is limited to your exchange account; set the "account" POST parameter to "all" to include your margin and lending accounts.

        :return: 
        """
        return self._private('returnCompleteBalances')


    def returnDepositAddresses(self):
        """
        Returns all of your deposit addresses

        :return: 
        """
        return self._private('returnDepositAddresses')


    def generateNewAddress(self, currency):
        """
        Generates a new deposit address for the currency specified by the "currency" POST parameter.
        :param currency: 
        :return: 
        """
        return self._private('generateNewAddress',{"currency":currency})


    def returnDepositsWithdrawals(self, start, end):
        return self._private("returnDepositsWithdrawals", {
            'start': create_timestamp(start),
            'end': create_timestamp(end),
        })


    def returnOpenOrders(self, currencyPair):
        """
        Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP"
        Inputs:
        currencyPair  The currency pair e.g. "BTC_XCP"
        Outputs:
        orderNumber   The order number
        type          sell or buy
        rate          Price the order is selling or buying at
        Amount        Quantity of order
        total         Total value of order (price * quantity)
        
        :param currencyPair: 
        :return: 
        """
        return self._private('returnOpenOrders',{"currencyPair":currencyPair})


    def returnAccountTradeHistory(self, currencyPair, start, end):
        """
        Returns your trade history for a given market, specified by the "currencyPair" POST parameter
        Inputs:
        currencyPair  The currency pair e.g. "BTC_XCP"
        Outputs:
        date          Date in the form: "2014-02-19 03:44:59"
        rate          Price the order is selling or buying at
        amount        Quantity of order
        total         Total value of order (price * quantity)
        type          sell or buy
        
        :param currencyPair: 
        :return: 
        """
        return self._private('returnTradeHistory', {
            "currencyPair":currencyPair,
            'start': create_timestamp(start),
            'end': create_timestamp(end)})


    def returnOrderTrades(self, orderNumber):
        """
        Returns all trades involving a given order, specified by the "orderNumber" POST parameter.
        If no trades for the order have occurred or you specify an order that does not belong to you,
        you will receive an error.
        :param orderNumber: 
        :return: 
        """
        return self._private('returnOrderTrades',{"orderNumber":orderNumber})


    def buy(self,currencyPair, rate, amount):
        """
        Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
        Inputs:
        currencyPair  The curreny pair
        rate          price the order is buying at
        amount        Amount of coins to buy
        Outputs:
        orderNumber   The order number
        
        :param currencyPair: 
        :param rate: 
        :param amount: 
        :return: 
        """
        return self._private('buy',{"currencyPair":currencyPair,"rate":rate,"amount":amount})


    def sell(self, currencyPair, rate, amount):
        """
        Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
        Inputs:
        currencyPair  The currency pair
        rate          price the order is selling at
        amount        Amount of coins to sell
        Outputs:
        orderNumber   The order number
        
        :param currencyPair: 
        :param rate: 
        :param amount: 
        :return: 
        """
        return self._private('sell',{"currencyPair":currencyPair,"rate":rate,"amount":amount})


    def cancel(self, currencyPair, orderNumber):
        """
        Cancels an order you have placed in a given market. Required POST parameters are "currencyPair" and "orderNumber".
        Inputs:
        currencyPair  The curreny pair
        orderNumber   The order number to cancel
        Outputs:
        succes        1 or 0
        
        :param currencyPair: 
        :param orderNumber: 
        :return: 
        """
        return self._private('cancelOrder',{"currencyPair":currencyPair,"orderNumber":orderNumber})


    def withdraw(self, currency, amount, address):
        """
        Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method, the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount", and "address". Sample output: {"response":"Withdrew 2398 NXT."}
        Inputs:
        currency      The currency to withdraw
        amount        The amount of this coin to withdraw
        address       The withdrawal address
        Outputs:
        response      Text containing message about the withdrawal
        
        :param currency: 
        :param amount: 
        :param address: 
        :return: 
        """
        return self._private('withdraw',{"currency":currency, "amount":amount, "address":address})


    def _post_process(self, item):
        # Add timestamps if there isnt one but is a datetime
        if('return' in item):
            if(isinstance(item['return'], list)):
                for x in xrange(0, len(item['return'])):
                    if(isinstance(item['return'][x], dict)):
                        if('datetime' in item['return'][x] and 'timestamp' not in item['return'][x]):
                            item['return'][x]['timestamp'] = float(create_timestamp(item['return'][x]['datetime']))

        convert_floats(item)
        return item

    def _api(self, type, params):
        try:
            params = dict((k,v) for k,v in params.iteritems() if v is not None)
        except AttributeError:
            params = dict((k,v) for k,v in params.items() if v is not None)

        if 'public' == type:
            uri = 'https://poloniex.com/public?' + urlencode(params)

            ret = urlopen(Request(uri))
            # jsonRet = json.loads(ret.read())

        if 'private' == type:
            post_data = urlencode(params)

            sign = hmac.new(self.Secret, post_data, hashlib.sha512).hexdigest()
            headers = {
                'Sign': sign,
                'Key': self.APIKey
            }

            ret = urlopen(Request('https://poloniex.com/tradingApi', post_data, headers))
            # jsonRet = json.loads(ret.read())
            # return self.post_process(jsonRet)

        if self.parseJson:
            res = json.loads(ret.read().decode('utf-8'))
        else:
            res = ret.read()

        if self.post_process:
            return self._post_process(res)
        else:
            return res


    def _private(self, command, params={}):
        params['command'] = command
        params['nonce'] = int(time.time()*1000)

        return self._api('private', params)

    def _public(self, command, params={}):
        params['command'] = command

        return self._api('public', params)

