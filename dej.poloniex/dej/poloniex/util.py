from datetime import datetime


def convert_floats_list(l):
    """
    Mutatively walk a list and convert strings to floats
    """
    for i in range(len(l)):
        if isinstance(l[i], dict):
            convert_floats_dict(l[i])
        elif isinstance(l[i], list):
            convert_floats_list(l[i])
        else:
            try:
                if type(l[i]) != int:
                    l[i] = float(l[i])
            except:
                try:
                    l[i] = datetime.strptime(l[i], '%Y-%m-%d %H:%M:%S')
                except:
                    pass


def convert_floats_dict(d):
    """
    Mutatively walk a dict and convert strings to floats
    """
    for k, v in d.iteritems():
        if isinstance(v, dict):
            convert_floats_dict(v)
        elif isinstance(v, list):
            convert_floats_list(v)
        else:
            try:
                if type(v) != int:
                    d[k] = float(v)
            except:
                try:
                    d[k] = datetime.strptime(v, '%Y-%m-%d %H:%M:%S')
                except:
                    pass
    return d


def convert_floats(x):
    """
    Mutatively walk given arg and convert strings to floats
    """
    if isinstance(x, dict):
        convert_floats_dict(x)
    elif isinstance(x, list):
        convert_floats_list(x)
    return x

