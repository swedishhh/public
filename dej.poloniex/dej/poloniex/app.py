import time
time.time()
import logging
import pandas as pd
from datetime import date
from arctic import Arctic
from dej.base import dt, dtd
from dej.base.manahl_arctic import check_quota
from dej.poloniex.api import Poloniex

logger = logging.getLogger(__name__)

START_DATE = dt(2014, 1, 1)


def exercise_api(api_key, secret):

    api = Poloniex(APIKey=api_key, Secret=secret)

    # public API
    api.returnTicker()
    api.return24hVolume()
    api.returnOrderBook('BTC_ZEC')
    api.returnTradeHistory('BTC_ZEC', start='2017-04-01 00:00:00', end='2017-04-01 00:02:00')
    api.returnChartData('BTC_ETH', period=86400, start='2017-04-01 00:00:00', end='2017-05-01 00:00:00')
    api.returnCurrencies()
    api.returnLoanOrders('LTC')

    # private API
    api.returnBalances()
    api.returnCompleteBalances()
    api.returnDepositAddresses()
    api.generateNewAddress('XMR')
    api.returnDepositsWithdrawals(start='2017-03-01 00:00:00', end='2017-05-01 00:00:00')
    api.returnOpenOrders('all')
    api.returnAccountTradeHistory('BTC_ZEC', start='2017-03-01 00:00:00', end='2017-05-01 00:00:00')
    api.returnAccountTradeHistory('all', start='2017-03-01 00:00:00', end='2017-05-01 00:00:00')
    api.returnOrderTrades(orderNumber=5315361422)
    [api.returnOpenOrders(x) for x in ['BTC_BELA', 'BTC_SYS']]
    [(k, v) for k, v in api.returnCompleteBalances().items() if v['btcValue'] != 0.0]


def replace_data(api, library, symbol, period=300, start=START_DATE):
    data = api.returnChartData(symbol, start=start, end=dt.now(), period=period)
    library.write(symbol, data)


def update_data(api, library, symbol, period=300, start=START_DATE, force_overwrite=False):
    if force_overwrite:
        logger.info('Overwriting %s, downloading from %s' % (symbol, start))
        replace_data(api, library, symbol, period, start)
    elif symbol not in library.list_symbols():
        logger.info('no data found for %s, downloading from %s' % (symbol, start))
        replace_data(api, library, symbol, period, start)
    else:
        data = library.read(symbol).data
        start = data.index[-1] + dtd(seconds=1)
        end = dt.now()
        logger.info('updating %s from %s to %s' % (symbol, start, end))
        new_data = api.returnChartData(symbol, start=start, end=end, period=period)
        logger.info('current: %s to %s' % (data.index[0], data.index[-1]))
        logger.info('new    : %s to %s' % (new_data.index[0], new_data.index[-1]))
        if (len(new_data) == 1) and (new_data.index[-1].date() == date(1970, 1, 1)):
            # it seems poloniex gives a single record from 1970 if no dej.poloniex found
            # so ignore it.
            logger.info('seems uptodate, not updating')
        else:
            updated_data = pd.concat([data, new_data])
            logger.info('updated: %s to %s' % (updated_data.index[0], updated_data.index[-1]))
            library.write(symbol, updated_data)


def download(arctic_host, library, symbols=None, include_delisted=False, force_overwrite=False):
    arctic = Arctic(arctic_host)

    if library not in arctic.list_libraries():
        arctic.initialize_library(library)

    check_quota(arctic, library, threshold=10, increment=10)
    lib = arctic[library]
    api = Poloniex()

    if not symbols:
        currencies_raw = api.returnCurrencies()
        currencies = sorted([k for k, v in currencies_raw.items() if (include_delisted or v['delisted'] == 0)])
        symbols = ['%s_%s' % ('BTC', currency) for currency in currencies if currency not in ['BTC', 'USDT']] + ['USDT_BTC', 'USDT_ETH']

    logger.info('downloading these symbols:\n%s' % '\n'.join(symbols))

    for symbol in symbols:
        try:
            update_data(api, lib, symbol, period=300, force_overwrite=force_overwrite)
        except Exception as ex:
            logger.warn('error updating %s, exception is %s' % (symbol, ex))

