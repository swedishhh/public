import os
from setuptools import setup

setup(
    name = "dej.base",
    version = "0.0.1",
    author = "",
    author_email = "",
    description = "",
    license = "",
    keywords = "",
    url = "",
    packages=['dej.base'],
    long_description='',
    install_requires=[
        'numpy>=1.12.0',
        'scipy>=0.18.0',
        'matplotlib==2.0.0',
        'pandas<=0.19.2',  # restriction for arctic 35
        'pymongo>=3.2.2',
        'arctic>=1.35.0',
        'Cython',  # for arctic
        'pytest',
    ]
)
