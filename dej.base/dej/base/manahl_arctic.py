import logging

logger = logging.getLogger(__name__)


def check_quota(arctic, library_name, threshold=10, increment=10):
    """
    Increase library quota (default by 10GB) if free_space is less than some threshold (default 10GB). 
    """
    arctic.check_quota(library_name)
    lib = arctic[library_name]
    quota = arctic.get_quota(library_name) / 1024 ** 3
    used_space = float(lib.stats()['totals']['size']) / 1024 ** 3
    free_space = quota - used_space
    logger.info('used=%.3fGB, free=%.3fGB, quota=%.3fGB' % (used_space, free_space, quota))
    if free_space < threshold:
        logger.info('increasing quota by %.3fGB' % increment)
        arctic.set_quota(library_name, (quota + increment) * 1024 ** 3)
